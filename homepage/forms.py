from django import forms

class StatusForm(forms.Form):

    status = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': "Any thougths ?",
        'required': True,
    }))
