# Generated by Django 3.0.3 on 2020-02-25 16:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='classyear',
            name='batch',
        ),
        migrations.AddField(
            model_name='classyear',
            name='year',
            field=models.CharField(choices=[('2019', '2019'), ('2018', '2018'), ('2017', '2017'), ('2016', '2016'), ('Others', 'Others')], default='NULL', max_length=255),
        ),
    ]
