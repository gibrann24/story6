from django.test import TestCase, Client
from django.urls import resolve
from datetime import datetime
from homepage.forms import StatusForm
from django.http import HttpRequest
from homepage.views import index
from homepage.models import Status
from datetime import datetime
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options

class homepageUnitTest(TestCase):
    def test_landing_page(self):
        response = Client().get("")
        self.assertEqual(response.status_code,200)

    def test_landing_page_template(self):
        response = Client().get("")
        self.assertTemplateUsed(response, 'index.html')

    def test_welcome_text(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Hello, how are you?",html_response)

    def test_models(self):
        Status.objects.create(status="abc", time = datetime.now())
        n = Status.objects.all().count()
        self.assertEqual(n,1)

    def test_form_valid(self):
        data = {'status':"abc","time": datetime.now()}
        status_form = StatusForm(data=data)
        self.assertTrue(status_form.is_valid())
        self.assertEqual(status_form.cleaned_data['status'], "abc")

    def test_form_post(self):
        test_str = 'abc'
        response_post = Client().post('', {'status':test_str,'time':datetime.now()})
        self.assertEqual(response_post.status_code,200)
        status_form = StatusForm(data={'status':test_str,'time':datetime.now()})
        self.assertTrue(status_form.is_valid())
        self.assertEqual(status_form.cleaned_data['status'], "abc")

#######################################################################################
#######################################################################################
#######################################################################################

# class FunctionalTest(TestCase):

#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         chrome_options.add_argument('--disable-dev-shm-usage')
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.get('http://localhost:8000')

#     def tearDown(self):
#         self.selenium.quit()

#     def test_open_page(self):
#         browser = self.selenium
#         self.assertIn("Welcome!",browser.title)
        
#     def test_status(self):
#         browser = self.selenium
#         str_input = "Functional test"
#         text_area = browser.find_element_by_id("id_status")
#         text_area.send_keys(str_input)
#         sub_button = browser.find_element_by_id("submit")
#         sub_button.click()
#         self.assertIn(str_input,browser.page_source)


